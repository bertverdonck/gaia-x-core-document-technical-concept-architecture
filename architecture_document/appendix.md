# Appendix

## A1

Examples of Attribute Categories per Self-Description in Gaia-X are
discussed in Appendix A.

- **Providers**: Every Provider of Service Offerings has to be
  registered as Provider and thus requires a Self-Description. The
  categories comprise identity, contact information, certification.

- **Nodes:** Self-Descriptions of Nodes describe relevant functional
  and non-functional attributes of Nodes as described in Section
  "Basic Architecture Elements". The Attribute Categories comprise
  availability, connectivity, hardware, monitoring, physical security
  and sustainability.

- **Software Assets:** Self-Descriptions of Software Assets describe
  Software Assets as defined in Section 2 (Conceptual Model).
  Attribute Categories for Software Assets are still under discussion
  and are not yet finalized.

- **Consumers (optional)**: Self-Descriptions of Consumers are
  optional, but may be required for accessing critical Data Assets
  and/or specific domains. Attribute categories for Consumers are
  still under discussion and are not yet finalized.

## A2

![](media/image14.png)

**Operational example Federated Trust Model**

0.  A Visitor accesses the Gaia-X Federated Trust, browses the Gaia-X
    Federated Catalogue and starts a Service search query. A list with
    possible services matching the service search criteria will be
    displayed to the Visitor.

1.  The Provider entity registers in Gaia-X. One of the mandatory fields
    is the input of the Identity System. An Identity System must confirm
    the Identity of the Provider.

2.  Existing Identifiers will be enabled for Gaia-X usage. Result: The
    Provider is verified and registered in Gaia-X. The Provider is able
    to register a Service in the Gaia-X Federated Catalogue, it is
    generated during Service Self-Description creation. The registered
    Service will be published to the Gaia-X Federated Catalogue and is
    publicly available.

3.  A Consumer registers in Gaia-X. One of the mandatory fields is the
    input of the Identity System. An Identity System must confirm the
    Identity of the Consumer and can be verified itself by Gaia-X.
    Existing Identifiers will be enabled for Gaia-X usage. Result: The
    Consumer is verified and registered in Gaia-X.

4.  The registered Consumer contacts the Service Provider to order a
    specific Service.

5.  The Provider AM checks the trustworthiness of the Consumer. The
    Gaia-X Federated Trust Component is used to check the Identity via
    the Identity System. The Gaia-X Federated Trust Component is used to
    verify the Service Access (e.g. required certifications of the
    Consumer to access health data).

    a.  Deny/Grant Access

    b.  Deny: The Provider AM will provide the result to the Consumer.

6.  Grant: The Provider AM will trigger the service orchestration engine
    to create the Service Instance for the Consumer (= Service
    Instantiation process). The Service Provider will forward the
    Service Instance Details to the Consumer.

7.  The Consumer is now able to use the ordered Service Instance. The
    Provider AM will check/verify for each access the identity of the
    Consumer using the Federated Trust Component to guarantee that the
    Consumer attribute matches the required ones (see step 6/7).

8.  The Consumer can offer - outside of the Gaia-X ecosystem - Services
    to their End-Users (not part of Gaia-X). These external offerings
    can rely on Gaia-X Service instances or can be enriched by data out
    of Gaia-X Services.
