## Data Ecosystem

A Data Ecosystem is a loose set of interacting actors that directly or indirectly consume, produce, or provide [data and other related resources](#data-asset).

### references

Oliveira, M. I. S., Lima, G. D. F. B., & Lóscio, B. F. (2019). Investigations into Data Ecosystems: a systematic mapping study. Knowledge and Information Systems, S.16
