## Continuous Automated Monitoring 

Process that automatically gathers and assesses information about the compliance of GAIA-X services, with regard to the GAIA-X Policy Rules and Architecture of Standards.

### alias
- CAM
