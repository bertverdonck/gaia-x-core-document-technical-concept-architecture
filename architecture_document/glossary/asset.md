## Asset

Element used to compose the [Service Offering], which does not expose an endpoint.

### reference

- <https://www.iso.org/obp/ui/#iso:std:iso-iec:27035:-3:ed-1:v1:en:term:3.1>
