## Catalogue

A Catalogue is an instance of the [Federation Service](#federation-services) [Federated Catalogue](#federated-catalogue) and present a list of [Service Offerings](#service-offering) available.

Catalogues are the main building blocks for the publication and discovery of a [Participant's](#participant) or [Service Offering's](#service-offering) [Self-Descriptions](#self-description).

### alias
- GAIA-X Catalogue
