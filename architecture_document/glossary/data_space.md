## Data Space

A Data Space is a virtual data integration concept defined as a set of participants and a set of relationships among them, where participants provide their data resources and computing services. 

Data Spaces have following design principles:

1. data resides in its sources;
2. only semantic integration of data and no common data schema;
3. nesting and overlaps are possible;
4. spontaneous networking of data, data visiting and coexistence of data are enabled. 

Within one Data Ecosystem, several Data Spaces can emerge.


### references
Franklin, M., Halevy, A., & Maier, D. (2005). From databases to dataspaces: a new abstraction for information management. ACM Sigmod Record, 34(4), 27-33.
