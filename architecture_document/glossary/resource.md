## Resource

A Resource is an internal building block, not available for order, used to compose [Service Offerings](#service-offering).  
Unlike an [Asset](#asset), it exposes endpoints.

Resource prominent attributes are the location - physical address, Autonomous System Number, network segment - and the jurisdiction affiliations.
